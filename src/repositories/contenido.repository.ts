import {DefaultCrudRepository} from '@loopback/repository';
import {Contenido, ContenidoRelations} from '../models';
import {PruebaDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ContenidoRepository extends DefaultCrudRepository<
  Contenido,
  typeof Contenido.prototype.id,
  ContenidoRelations
> {
  constructor(
    @inject('datasources.prueba') dataSource: PruebaDataSource,
  ) {
    super(Contenido, dataSource);
  }
}
