import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {Contenido} from '../models';
import {ContenidoRepository} from '../repositories';

export class ContenidoController {
  constructor(
    @repository(ContenidoRepository)
    public contenidoRepository : ContenidoRepository,
  ) {}

  @post('/contenidos', {
    responses: {
      '200': {
        description: 'Contenido model instance',
        content: {'application/json': {schema: getModelSchemaRef(Contenido)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contenido, {
            title: 'NewContenido',
            exclude: ['id'],
          }),
        },
      },
    })
    contenido: Omit<Contenido, 'id'>,
  ): Promise<Contenido> {
    return this.contenidoRepository.create(contenido);
  }

  @get('/contenidos/count', {
    responses: {
      '200': {
        description: 'Contenido model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(Contenido) where?: Where<Contenido>,
  ): Promise<Count> {
    return this.contenidoRepository.count(where);
  }

  @get('/contenidos', {
    responses: {
      '200': {
        description: 'Array of Contenido model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Contenido, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(Contenido) filter?: Filter<Contenido>,
  ): Promise<Contenido[]> {
    return this.contenidoRepository.find(filter);
  }

  @patch('/contenidos', {
    responses: {
      '200': {
        description: 'Contenido PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contenido, {partial: true}),
        },
      },
    })
    contenido: Contenido,
    @param.where(Contenido) where?: Where<Contenido>,
  ): Promise<Count> {
    return this.contenidoRepository.updateAll(contenido, where);
  }

  @get('/contenidos/{id}', {
    responses: {
      '200': {
        description: 'Contenido model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(Contenido, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(Contenido, {exclude: 'where'}) filter?: FilterExcludingWhere<Contenido>
  ): Promise<Contenido> {
    return this.contenidoRepository.findById(id, filter);
  }

  @patch('/contenidos/{id}', {
    responses: {
      '204': {
        description: 'Contenido PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(Contenido, {partial: true}),
        },
      },
    })
    contenido: Contenido,
  ): Promise<void> {
    await this.contenidoRepository.updateById(id, contenido);
  }

  @put('/contenidos/{id}', {
    responses: {
      '204': {
        description: 'Contenido PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() contenido: Contenido,
  ): Promise<void> {
    await this.contenidoRepository.replaceById(id, contenido);
  }

  @del('/contenidos/{id}', {
    responses: {
      '204': {
        description: 'Contenido DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.contenidoRepository.deleteById(id);
  }
}
