import {Entity, model, property} from '@loopback/repository';

@model()
export class Contenido extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  titulo: string;

  @property({
    type: 'string',
    required: true,
  })
  descripcion: string;

  @property({
    type: 'date',
    required: true,
  })
  fecha: string;

  @property({
    type: 'string',
  })
  comentarios?: string;


  constructor(data?: Partial<Contenido>) {
    super(data);
  }
}

export interface ContenidoRelations {
  // describe navigational properties here
}

export type ContenidoWithRelations = Contenido & ContenidoRelations;
